package iterableCollection.listCollection;

import iterableCollection.model.Person;

import java.util.*;

public class MainQueue {
    public static void main(String[] args) {
        // Queue
        Queue<String> queue = new ArrayDeque<>(10);
        queue.offer("Eko");
        queue.offer("Kurniawan");
        queue.offer("Khannedy");

        for (String next = queue.poll(); next != null; next = queue.poll()){
            System.out.println(next);
        }

        System.out.println(queue.size());
        System.out.println();

        // Priority Queue
        Queue<String> queue1 = new PriorityQueue<>();
        queue1.offer("Eko");
        queue1.offer("Kurniawan");
        queue1.offer("Khannedy");

        for (String next = queue1.poll(); next != null; next = queue1.poll()){
            System.out.println(next);
        }

        System.out.println(queue1.size());
        System.out.println();

        // Deque
        Deque<String> stack = new LinkedList<>();

        stack.offerLast("Eko");
        stack.offerLast("Kurniawan");
        stack.offerLast("Khannedy");

        for (var item = stack.pollLast(); item != null; item = stack.pollLast()){
            System.out.println(item);
        }

        System.out.println();
    }
}
