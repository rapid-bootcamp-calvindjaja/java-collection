package iterableCollection.listCollection;

import java.util.*;

public class MainMap {
    public static void main(String[] args) {
        //HashMap
        Map<String,String> map = new HashMap<>();
        map.put("firstName","Eko");
        map.put("middleName","Kurniawan");
        map.put("lastName","Khannedy");

        System.out.println(map.get("firstName"));
        System.out.println(map.get("middleName"));
        System.out.println(map.get("lastName"));
        System.out.println();

        //Weak HashMap
        Map<Integer,Integer> integerMap = new WeakHashMap<>();
        for (int i = 0; i < 1000000; i++) {
            integerMap.put(i,i);
        }
        System.gc();
        System.out.println(integerMap.size());
        System.out.println();

        //Identity HashMap
        String key1 = "name.first";
        String name = "name";
        String first = "first";
        String key2 = name + "." + first;
        Map<String, String> map1 = new IdentityHashMap<>();
        map1.put(key1, "Eko Kurniawan");
        map1.put(key2, "Eko Kurniawan");

        System.out.println(map1);
        System.out.println(map1.size());
        System.out.println();

        //Linked HashMap
        Map<String,String> map2 = new LinkedHashMap<>();
        map2.put("Eko","Eko");
        map2.put("Kurniawan","Kurniawan");
        map2.put("Khannedy","Khannedy");

        for (var key: map2.keySet()) {
            System.out.println(key);
        }
        System.out.println();

        EnumMap<Level, String> map3 = new EnumMap<>(Level.class);
        map3.put(Level.FREE,"Gratis");
        map3.put(Level.PREMIUM, "Bayar");

        System.out.println(map3.get(Level.FREE));
        System.out.println(map3.get(Level.STANDARD));
        System.out.println(map3.get(Level.PREMIUM));
    }

    public static enum Level {
        FREE, STANDARD, PREMIUM, VIP;
    }
}
